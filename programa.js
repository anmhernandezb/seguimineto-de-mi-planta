
 $(document).ready(function() {
	$(".menu-icon").on("click", function() {
		  $("nav ul").toggleClass("showing");
	});
});

// Scrolling Effect

$(window).on("scroll", function() {
	if($(window).scrollTop()) {
		  $('nav').addClass('black');
	}

	else {
		  $('nav').removeClass('black');
	}
})

let mymap = L.map('mapid');
mymap.setView([4.857989, -74.057312], 17);

let miProveedor =L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
});

miProveedor.addTo(mymap);
let miMarcador = L.marker([4.8577969348251955, -74.05729080002946]);

miMarcador.bindPopup("<b>Granja Urbana Angelo Hernández </b><br>Planta: Hierbabuena<br>Fecha de plantación: 2020/12/18.").openPopup();
miMarcador.addTo(mymap);

let polygon=L.polygon(

	[
		[4.857782985332913,-74.05731707811356],
		[4.857752918870015,-74.05724868178368],
		[4.857879198005216,-74.05718833208084],
		[4.857910600749461,-74.05725941061974],
		[4.857782985332913,-74.05731707811356]
	]

);

polygon.bindPopup("<b>Datos</b><br>Nombre agricultor: Angelo Hernández<br>Planta: Hierbabuena<br>Nombre científico: Mentha spicata<br>Fecha de plantacion:2020/12/18<br>Ubicación: 4.6574503977753805, -74.09359854567951 ")
polygon.addTo(mymap);

//geoJSoN : describe objetos geograficos
//puntos (point)
//lineas (lineString)
//polígonos (polygon)
//Multiples lineas (MultilineString)
//Multiples polígonos (MultiPolygon)
//Colección geometrías (Geometry collection)

//Entidad geográfica (Features): Objeto geográfico + Metadata

//GSe recomienda que un GeoJSON define features, no solo la geometría

